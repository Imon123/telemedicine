<?php
include '../header.php';
include 'dsession.php';

$dinfo = $doctor->get_doctor_allinfo_by_id($did);
?>

<section class="patient-area sec-pdd1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <?php
                    if (empty($dinfo['present_address']) || empty($dinfo['permanent_address']) || empty($dinfo['profile_image'])) {
                        include_once 'update-profile.php';
                    } else {
                        ?>
                        <div class="col-xs-12 col-sm-3">
                            <div class="leftsidebar">
                                <ul> 
                                    <li>
                                        <a <?php if ($current_page == 'index'): echo 'class="menu-active"'; endif; ?> href="index">My State</a>
                                    </li><li>
                                        <a <?php if ($current_page == 'doctor-profile'): echo 'class="menu-active"'; endif; ?> href="doctor-profile">View Profile</a>
                                    </li><li>
                                        <a <?php if ($current_page == 'edit-profile'): echo 'class="menu-active"'; endif; ?> href="edit-profile">Edit Profile</a>
                                    </li>                      
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="mystate">
                                <?php
                                if ($dinfo['approval_status'] == 0) {
                                    echo '<div class="profile-approve"><span class="success">Thank you for submission your profile</span><span>Your profile is under review. You should hear back within 48 hours.</span> <span>You will be able to activities once your profile is approved</span></div>';
                                } else {
                                    echo '<p><b>Profile Status:</b> Approved</p>';
                                }
                                ?>
                                <table class="table table-inside" style="width:90%">
                                    <tbody>
                                        <tr id="latest_app">
                                            <th>New Appontment</th>
                                            <td>0</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <th><a href="">Emargency Appontment</a></th>
                                            <td><a href="">0</a></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <th><a href="">Appontment Archive</a></th>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
<?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include '../footer.php'; ?>   