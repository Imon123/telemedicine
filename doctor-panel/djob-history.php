<?php 
    include '../header.php';
    include 'dsession.php';
 ?>
 <section class="patient-area sec-pdd1">
    <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <?php include 'sidebar.php'; ?>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="dupdate-profile">
                <div id="display_jobhistory"></div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- doctor job history modal -->
<div class="modal fade doctor_id-modal" data-keyboard="false" data-backdrop="static" id="docjobhistorymodal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                <form>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="doc_office_name">Office Name <span class="required">*</span></label>
                                <input type="hidden" id="d_id" value="<?php echo $did; ?>">
                                <input type="text" class="form-control" id="doc_office_name" placeholder="Office Name">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="start_date">Job Period<span class="required">*</span></label>
                                <input type="text" class="form-control" id="start_date" placeholder="From">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="end_date"></span></label>
                                <input type="checkbox" class="continue" id="continue" value="continue">&nbsp; Currently Working
                                <div class="remove_date">
                                    <input type="text" class="form-control" id="end_date" placeholder="To">
                                </div>
                                <div class="text_continue">
                                    <input type="text" class="form-control" disabled id="end_date_text" value="Continuing">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-descriptiongroup">
                                <label for="office_address">Office Address</label>
                                <textarea class="form-control" id="office_address" style="resize: none;" placeholder="Office Addres"></textarea>
                            </div>
                        </div>
                        
                        <div class="col-xs-12">
                            <hr>
                            <button type="submit" onclick="dsaveJobhistoryData()" class="doct-submit button btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php 
    include '../footer.php';
 ?>