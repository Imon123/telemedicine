<?php
include '../header.php';
include 'dsession.php';

$dinfo = $doctor->get_doctor_allinfo_by_id($did);
?>

<section class="patient-area sec-pdd1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                        <div class="leftsidebar">
                            <ul> 
                                <li>
                                    <a <?php if ($current_page == 'index'): echo 'class="menu-active"'; endif; ?> href="index">My State</a>
                                </li>
                                <li>
                                    <a <?php if ($current_page == 'doctor-profile'): echo 'class="menu-active"'; endif; ?> href="doctor-profile">View Profile</a>
                                </li>
                                <li>
                                    <a <?php if ($current_page == 'edit-profile'): echo 'class="menu-active"'; endif; ?> href="edit-profile">Edit Profile</a>
                                </li>                      
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="mystate">
                            <h2>Latest Appointment List</h2>
                            <table class="table table-inside">
                                <thead>
                                    <tr>
                                        <th>SL NO.</th>
                                        <th>Patient Name</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $result = $doctor->latest_appointment_list($id = null, $did);
                                        if($result):
                                            $i = 0;
                                        while ( $arow = $result->fetch_assoc()) {  
                                            $i++;
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $arow['first_name'] .' '.$arow['last_name']; ?></td>
                                        <td><?= $fm->getDate($arow['created_at'], 'jS M Y,  g:i:s'); ?></td>
                                        <td><a href="appointment-details?ap-id=<?= $arow['appoint_id']?>">View Details</a></td>
                                    </tr>
                                <?php } else: ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include '../footer.php'; ?>   
