<?php 
    include '../header.php';
    include 'dsession.php';
 ?>
 <section class="patient-area sec-pdd1">
    <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-3">
        <?php include 'sidebar.php'; ?>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="dupdate-profile">
                <div id="status" style="display: none;"></div>
                <div id="display_training"></div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- dtraning modal -->
<div class="modal fade dtraning-modal" data-keyboard="false" data-backdrop="static" id="dtrainingmodal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                <form>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="training_title">Training Title <span class="required">*</span></label>
                                <input type="hidden" id="d_id" value="<?php echo $did; ?>">
                                <input type="text" class="form-control" id="training_title">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="institute_name">Institute Name <span class="required">*</span></label>
                                <input type="text" class="form-control" id="institute_name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="address">Institute Address <span class="required">*</span></label>
                                <textarea class="form-control" id="address" style="resize: none;"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-descriptiongroup">
                                <label for="">Description (optional)</label>
                                <textarea class="form-control" id="description" style="resize: none;"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 clearfix">
                            <button type="submit" onclick="dsaveTrainingData()" class="doct-submit button btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php 
    include '../footer.php';
 ?>