<?php
include 'header.php';
include 'sidebar.php';
if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['patient_id'])) {
    $patient_id = $_GET['patient_id'];
    $patient_details_by_id = $admin_mg->get_patient_profile_by_id($patient_id);
}

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['new_user'])) {
    $patient_id = $_POST['new_user'];
    $admin_mg->read_notification($patient_id);
    $count = $admin_mg->notification_count();
$notifications = $admin_mg->notifications();
    $patient_details_by_id = $admin_mg->get_patient_profile_by_id($patient_id);
}
?>

<section class="content-wrapper">
    <div class="container" style="padding: 50px 0px">
        <div class="row"> 
            <div class="col-xs-12 height-control">
                <div class="row">

                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <?php if (!empty($patient_details_by_id)) { ?>
                            <div class="col-xs-4 col-xs-offset-8" style="padding-bottom: 30px">
                                <table class="table-hover table-bordered">
                                    <tr>
                                        <td><img src="../<?= $patient_details_by_id['profile_img'] ?>" width="200" height="200"/></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-xs-12">
                                <div style="border-bottom: 2px solid #000;">

                                </div>
                                <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Patient Personal Details</u></h4>
                                <table class="table-hover">
                                    <tr>
                                        <td>Name</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['first_name'] . " " . $patient_details_by_id['last_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Father Name</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['father_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Mother Name</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['mother_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email Address</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Mobile No.</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['mobile_no'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Height</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['height'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Weight</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['weight'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Blood Group</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['blood_group'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>NID </td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['nid'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['gender'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['address'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Age</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['birthday'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Account Type</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['account_type'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Expire Date</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?= $patient_details_by_id['expire_date'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Active Status</td>
                                        <td class="col-sm-1">:</td>
                                        <td><?php if ($patient_details_by_id['active_status'] == 0) { ?> Not Active
                                            <?php } else { ?>
                                                Active<?php } ?></td>
                                    </tr>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>