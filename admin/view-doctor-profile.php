<?php
    include 'header.php';
    include 'sidebar.php';
    if($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['doctor_id'])){
        $doctor_id = $_GET['doctor_id'];
        $doctor_details_by_id = $doctor->doctor_personal_details($doctor_id);
        $doctor_education_by_id = $doctor->doctor_education_info($doctor_id);
        $doctor_training_by_id = $doctor->doctor_training_info($doctor_id);
        $doctor_job_history_by_id = $doctor->doctor_job_history($doctor_id);
    }
    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['approved_him'])){
    $approved_him = $_POST['approved_him'];
    $admin_mg->approve_this_doctor($approved_him);
    $doctor_details_by_id = $doctor->doctor_personal_details($doctor_id);
        $doctor_education_by_id = $doctor->doctor_education_info($doctor_id);
        $doctor_training_by_id = $doctor->doctor_training_info($doctor_id);
        $doctor_job_history_by_id = $doctor->doctor_job_history($doctor_id);
}
?>

<section class="content-wrapper">
    <div class="container" style="padding: 50px 0px">
        <div class="row"> 
            <div class="col-xs-12 height-control">
                <div class="row">
                    
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <?php if(!empty($doctor_details_by_id)){ ?>
                        <div class="col-xs-8">
                        <?php
                            if($doctor_details_by_id['approval_status'] == 0){?>
                                <a href="" class="btn-primary btn-group btn-lg"> Approved Me</a>
                         <?php   }else{?>
                             <a href="" class="btn-primary btn-group btn-lg"> Already Approved</a>
                         <?php }
                        ?>
                            <table class="table-hover">
                                <tr>
                                    <td><h1 class="text-bold" style="color: blue; font-weight: bold"><?= $doctor_details_by_id['first_name'] . ' ' . $doctor_details_by_id['last_name'] ?></h1></td>
                                </tr>
                                <tr>
                                    <td>Address : <?= $doctor_details_by_id['present_address'] ?></td>
                                </tr>
                                <tr>
                                    <td>Mobile No. : <?= $doctor_details_by_id['mobile_no'] ?></td>
                                </tr>
                                <tr>
                                    <td>Email : <?= $doctor_details_by_id['email'] ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-4" style="padding-bottom: 30px">
                            <table class="table-hover table-bordered">
                                <tr>
                                    <td><img src="../<?= $doctor_details_by_id['profile_image'] ?>" width="200" height="200"/></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-12">
                            <div style="border-bottom: 2px solid #000;">

                            </div>
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Personal Details</u></h4>
                            <table class="table-hover">
                                <tr>
                                    <td>Identity No</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_details_by_id['idnetity_no'] ?></td>
                                </tr>
                                <tr>
                                    <td>National ID Or Passport No.</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_details_by_id['nid_passport'] ?></td>
                                </tr>
                                <tr>
                                    <td>Birth Date</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_details_by_id['birthday'] ?></td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_details_by_id['gender'] ?></td>
                                </tr>
                                <tr>
                                    <td>Specialists</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_details_by_id['specialists'] ?></td>
                                </tr>
                                <tr>
                                    <td>Present Address</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_details_by_id['present_address'] ?></td>
                                </tr>
                                <tr>
                                    <td>Permanent Address</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_details_by_id['permanent_address'] ?></td>
                                </tr>
                                <tr>
                                    <td>Self About</td>
                                    <td class="col-sm-1">:</td>
                                    <td><?= $doctor_details_by_id['self_about'] ?></td>
                                </tr>
                            </table>
                        </div>
                        <?php } 
                                if (!empty($doctor_education_by_id)) {?>
                        <div class="col-xs-12">
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Academic Qualification</u></h4>
                            <table class="table-hover text-center table-bordered" style="min-width: 100%">
                                <tr>
                                    <th class="text-center">Exam Title</th>
                                    <th class="text-center">Degree Form</th>
                                    <th class="text-center">Concentration/Major</th>
                                    <th class="text-center">Institute</th>
                                    <th class="text-center">Passing Year</th>
                                    <th class="text-center">Result</th>
                                    <th class="text-center">Duration</th>
                                </tr>
                                <?php
                                while ($doctor_education = $doctor_education_by_id->fetch_assoc()) {
                                ?>
                                <tr>
                                    <td><?= $doctor_education['degree_title'] ?></td>
                                    <td><?= $doctor_education['degree_form'] ?></td>
                                    <td><?= $doctor_education['major_subject'] ?></td>
                                    <td><?= $doctor_education['institute_name'] ?></td>
                                    <td><?= $doctor_education['passing_year'] ?></td>
                                    <td>Passed</td>
                                    <td></td>
                                </tr>
                                <?php }  ?>
                            </table>
                        </div>
                        <?php } 
                                if(!empty($doctor_training_by_id)){?>
                        <div class="col-xs-12">
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Training Summary</u></h4>
                            <table class="table-hover text-center table-bordered" style="min-width: 100%">
                                <tr>
                                    <th class="text-center">Training Title</th>
                                    <th class="text-center">Institute</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">Training Topic</th>
                                </tr>
                                <?php
                                while($doctor_training = $doctor_training_by_id->fetch_assoc()){
                                ?>
                                <tr>
                                    <td><?= $doctor_training['training_title'] ?></td>
                                    <td><?= $doctor_training['institute_name'] ?></td>
                                    <td><?= $doctor_training['address'] ?></td>
                                    <td><?= $doctor_training['description'] ?></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <?php }
                                if(!empty($doctor_job_history_by_id)){ ?>
                        <div class="col-xs-12">
                            <h4 style="font-weight: bold; background-color: #E6E6E6; padding: 2px"><u>Experience Summary</u></h4>
                            <table class="table-hover text-center table-bordered" style="min-width: 100%">
                                <tr>
                                    <th class="text-center">Office Name</th>
                                    <th class="text-center">Office Address</th>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">End Date</th>
                                </tr>
                                <?php
                                while($doctor_job_history_info = $doctor_job_history_by_id->fetch_assoc()){
                                ?>
                                <tr>
                                    <td><?= $doctor_job_history_info['doc_office_name'] ?></td>
                                    <td><?= $doctor_job_history_info['office_address'] ?></td>
                                    <td><?= $doctor_job_history_info['start_date'] ?></td>
                                    <td><?= $doctor_job_history_info['end_date'] ?></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>