<?php 
    include 'header.php'; 
    $ptLogin = Session::session_get('ptLogin');
    $dLogin = Session::session_get('dLogin');
    if($ptLogin == true){
        header("Location:patient-panel");
    }elseif($dLogin == true){
        header("Location:doctor-panel");
    } 
?>
<section id="account-option" class="accoption-wrapp sec-pdd2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="account-title text-center">
                    <h4>Create Your Account</h4>
                    <p>Please choose an option</p>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="ac-pa actcnt">
                            <img src="images/ac.png" alt="">
                            <h5>Doctor</h5>
                            <p>Find the best candidates in the fastest way</p>
                            <a href="#doctormodal" data-toggle="modal" class="crt-acc">Create account</a>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="ac-pa actcnt mbbg">
                            <img src="images/ac.png" alt="">
                            <h5>Patient</h5>
                            <p>Find the best candidates in the fastest way</p>
                            <a href="#modaldaftar" data-toggle="modal"  class="crt-acc cambc">Create account</a>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="ac-pa actcnt rbbg">
                            <img src="images/ac.png" alt="">
                            <h5>Diagnostic</h5>
                            <p>Find the best candidates in the fastest way</p>
                            <a href="clinic_registration" class="crt-acc carbc">Create account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Doctor account modal -->
<div class="modal fade doctor-modal" data-keyboard="false" data-backdrop="static" id="doctormodal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div id="doctor-form" class="doctor-wrapp">
                    <div class="form-outer">
                        <h2>Create your account now</h2>
                        <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                        <hr>
                        <div id="doct_notify"></div>
                        <form name="doctor_reg" id="doctorsignup">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name <span class="required">*</span></label>
                                        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name <span class="required">*</span></label>
                                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="idnetity_no">Identity Number <span class="required">*</span></label>
                                        <input type="text" name="idnetity_no" class="form-control" id="idnetity_no" placeholder="Identity Number">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="nidpassport">NID or Passport Number <span class="required">*</span></label>
                                        <input type="text" name="nidpassport" class="form-control" id="nidpassport" placeholder="NID or Passport Number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email Address <span class="required">*</span></label>
                                        <input type="text" name="email" class="form-control" id="email" placeholder="Email Address">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="mobile_no">Mobile Number <span class="required">*</span></label>
                                        <input type="text" name="mobile_no" class="form-control" id="mobile_no" placeholder="Mobile Number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="gender">Gender <span class="required">*</span></label>
                                        <select name="gender" class="form-control" id="gender">
                                            <option value="">Choose Gender </option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="docbirthday">Birth Date <span class="required">*</span></label>
                                        <input type="text" name="docbirthday" class="form-control" id="docbirthday" placeholder="mm/dd/yyyy">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3>Choose a Password <span class="required">*</span></h3>
                                    <hr>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" class="form-control" id="password">
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <label for="repassword">Confirm Password</label>
                                    <input type="password" name="repassword" class="form-control" id="repassword">
                                </div>
                                <div class="col-xs-12">
                                    <hr>
                                  <button type="submit" class="doct-submit button btn-primary" id="doct_button" name="btn" value="ptbutton">Sign Up</button>
                                </div>
                                
                            </div>
                        </form>
                        <div id="erro_notify"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        

  <!-- Patient Modal -->
  <div class="patient-modal">
  <div class="modal fade" data-keyboard="false" data-backdrop="static" id="modaldaftar" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <div class="main-row">
                <div class="form-wrapper">
                <h2 class="modal-title">Your Details</h2>
                <button type="button" class="btn md-close" data-dismiss="modal">Close</button>
                <hr>
                <form name="patient_reg" id="ptsignup">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="main-agileits form-group clearfix">
                                <h3>Account Type</h3>
                                <div class="four w3grids-agile">
                                    <input type="radio" name="r1" data-pvalue="Premium" value="premium" id="r1" checked>
                                    <label for="r1">
                                        <h4>Premium</h4>
                                    </label>
                                </div>
                                <div class="four w3grids-agile">
                                    <input type="radio" name="r1" data-trial="Free Trial" value="free trial" id="r3">
                                    <label for="r3">
                                        <h4>Free Trail</h4>
                                    </label>
                                </div> 
                                <input type="hidden" id="acc_type">
                            </div>
                        </div>
                    </div>
                    <hr>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pfirst_name">First Name <span class="required">*</span></label>
                                    <input type="text" name="pfirst_name" class="form-control" id="pfirst_name" placeholder="First Name">

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="plast_name">Last Name <span class="required">*</span></label>
                                    <input type="text" name="plast_name" class="form-control" id="plast_name" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <label for="father_name">Father Name <span class="optional">(Optional)</span></label>
                                <input type="text" name="father_name" class="form-control" id="father_name" placeholder="Father Name">
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="mother_name">Mother Name <span class="optional">(Optional)</span></label>
                                <input type="text" name="mother_name" class="form-control" id="mother_name" placeholder="Mother Name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <label for="pemail">Email Address <span class="required">*</span></label>
                                <input type="text" name="pemail" class="form-control" id="pemail" placeholder="Email Address">
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="pmobile_no">Mobile Number <span class="required">*</span></label>
                                <input type="text" name="pmobile_no" class="form-control" id="pmobile_no" placeholder="Mobile Number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="height">Height <span class="required">*</span></label>
                                    <input type="text" name="height" class="form-control" id="height" placeholder="Height">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="weight">Weight <span class="required">*</span></label>
                                    <input type="text" name="weight" class="form-control" id="weight" placeholder="Weight">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="blood_group">Blood Group <span class="optional">(Optional)</span></label>
                                    <select name="blood_group" class="form-control" id="blood_group">
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nid">NID or Passport NO. <span class="optional">(Optional)</span></label>
                                    <input type="text" name="nid" class="form-control" id="nid" placeholder="NID or Passport Number">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label for="pgender">Gender <span class="required">*</span></label>
                                <select name="pgender" class="form-control" id="pgender">
                                    <option value="">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Others">Others</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <label for="pbirthday">Enter your age <span class="required">*</span></label>
                                    
                                    <select name="pbirthday" class="form-control" id="pbirthday">
                                        <option value="">Select Age</option>
                                        <?php 
                                        $j = 0;
                                        for($j = 0; $j<=12; $j++){ ?>
                                            <option value="<?php echo $j." "."Months";?>"><?php echo $j." "."Months";?></option>
                                        <?php }
                                        $i = 0;
                                        for($i = 1; $i<=110; $i++){ ?>
                                            <option value="<?php  echo $i." "."Years";?>"><?php echo $i." "."Years";?></option>
                                       <?php  } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <label for="address">Address <span class="required">*</span></label>
                                <textarea class="form-control" id="address" name="address" style="resize: none"></textarea>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                         <div class="col-xs-12">
                            <h3>Choose a Password</h3>
                         </div>
                     </div>
                     <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="ppassword">Password <span class="required">*</span></label>
                                <input type="password" name="ppassword" class="form-control" id="ppassword">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="repassword">Confirm Password <span class="required">*</span></label>
                                <input type="password" name="repassword" class="form-control" id="repassword">
                            </div>  
                        </div>  
                    </div>
                    <button type="submit" class="pt-submit button btn-primary" id="pt_button" name="btn" value="ptbutton">Sign Up</button>
                </form>
                <div id="pt_notify"></div>                   
                </div>
            </div>  
        
        </div>
      </div>
      
    </div>
  </div>
  </div>
 <?php include 'footer.php'; ?>