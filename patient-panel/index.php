<?php
include '../header.php';
include 'psession.php';
?>
<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-3">
                <?php include './patient-sidebar.php'; ?>
            </div>
            <div class="col-xs-9">
                <div class="mystate">
                    <div class="row">
                     <?php include 'membership-message.php'; ?>
                     <?php include 'membership-stauts.php'; ?>  
                    </div>
                    <div class="row">
                                <table class="table table-inside" style="width:90%">
                                    <tbody>
                                        <tr id="new_message">
                                            <th>New Message</th>
                                            <td>0</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <th><a href="">Previous Message</a></th>
                                            <td><a href="">0</a></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include '../footer.php';?>