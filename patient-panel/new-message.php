<?php
include '../header.php';
include 'psession.php';
?>
<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-3">
                <?php include './patient-sidebar.php'; ?>
            </div>
            <div class="col-xs-9">
                <div class="mystate">
                    <div class="row">
                     <?php include 'membership-message.php'; ?>
                     <?php include 'membership-stauts.php'; ?>  
                    </div>
                    <h2>New Message List</h2>
                    <table class="table table-inside">
                        <thead>
                            <tr>
                                <th>SL NO.</th>
                                <th>Doctor Name</th>
                                <th>Spacialist</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $result = $patient->new_message_list($id = null, $pid);
                                if($result):
                                    $i = 0;
                                while ( $arow = $result->fetch_assoc()) {  
                                    $i++;
                            ?>
                            <tr>
                                <td><?= $i; ?></td>
                                <td><?= $arow['first_name'] .' '.$arow['last_name']; ?></td>
                                <td><?= $arow['specialists']; ?></td>
                                <td><?= $fm->getDate($arow['created_at'], 'jS M Y,  g:i:s'); ?></td>
                                <td><a href="appointment-details?ap-id=<?= $arow['appoint_id']?>">View Details</a></td>
                            </tr>
                        <?php } else: ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include '../footer.php';?>