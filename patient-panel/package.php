<?php
include '../header.php';
include 'psession.php';
?>
<div class="container sec-pdd1">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-3">
                <?php include './patient-sidebar.php'; ?>
            </div>
            <div class="col-xs-9">
                <div class="row">
                    <?php include 'membership-message.php'; ?>
                    <div id='succss_message'></div>
                    <div class="member-package" id="member_package">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="acc-info">
                                    <p><strong>Our bank Account no. below:</strong></p>
                                    <span><strong>bKash:</strong> 01866559988</span>
                                    <span><strong>Dbbl Mobile:</strong> 01866559988</span>
                                    <span><strong>Dbbl Bank A/C:</strong> 01866559988</span>
                                </div>
                            </div>
                        </div>

                        <form>
                        <div class="row">
                        <div class="col-md-12">
                            <h3>Package</h3>
                            <div class="row">
                            <div class="col-sm-6">
                                <div class="radio">
                                <label>
                                  <input type="radio" name="radios" class="track-order-change" id="firstRadio" value="6 months">
                                   <h4 class="position_radio">10000 TK/ 6 Months</h4>
                                </label>
                              </div>

                              <div class="panel-collapse collapse" id="firstAccordion">
                                <div>
                                  <label for="months6">Transection ID<span class="required">*</span></label>
                                  <input type="text" class="form-control" id="months6">
                                  <input type="hidden" class="form-control" id="amount6" value="10000">
                                </div>
                              </div>  
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                          <div class="radio">
                            <label>
                              <input type="radio" name="radios" class="track-order-change" id="secondRadio" value="1 year">
                              <h4 class="position_radio">18000 TK/ 1 Year</h4>
                            </label>
                          </div>

                          <div class="panel-collapse collapse" id="secondAccordion">
                            <div>
                             <label for="year1">Transection ID<span class="required">*</span></label>
                            <input type="test" class="form-control" id="year1">
                            <input type="hidden" class="form-control" id="amount1" value="18000">
                            </div>
                          </div> 
                            </div> 
                        </div>
                            <button type="submit" class="pt-submit button btn-primary" onclick="ptpackageSubmit()" value="ptbutton">Submit</button>
                        </div>
                    </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include '../footer.php';?>