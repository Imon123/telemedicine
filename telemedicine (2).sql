-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2017 at 06:41 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telemedicine`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(42) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `fullname`, `username`, `email`, `password`, `createdate`) VALUES
(1, 'Azizul Haque', 'azizul', 'azizul@gmail.com', '131ddb54f8a94da689fb1565372cdfb9d33b2106', '2017-07-28 18:49:38');

-- --------------------------------------------------------

--
-- Table structure for table `deducations`
--

CREATE TABLE `deducations` (
  `dedu_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `degree_title` varchar(255) NOT NULL,
  `degree_form` varchar(50) NOT NULL,
  `institute_name` text NOT NULL,
  `passing_year` varchar(4) NOT NULL,
  `major_subject` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deducations`
--

INSERT INTO `deducations` (`dedu_id`, `d_id`, `degree_title`, `degree_form`, `institute_name`, `passing_year`, `major_subject`) VALUES
(51, 8, 'Doctor of Medicin', 'MBBS', 'Dhaka Medical', '2001', 'Medicine');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `d_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `idnetity_no` varchar(50) NOT NULL,
  `nid_passport` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthday` date NOT NULL,
  `specialists` varchar(255) NOT NULL,
  `password` varchar(42) NOT NULL,
  `present_address` text NOT NULL,
  `permanent_address` text NOT NULL,
  `self_about` text NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `active_status` tinyint(4) DEFAULT '0',
  `online_status` tinyint(4) NOT NULL DEFAULT '0',
  `approval_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`d_id`, `first_name`, `last_name`, `idnetity_no`, `nid_passport`, `email`, `mobile_no`, `gender`, `birthday`, `specialists`, `password`, `present_address`, `permanent_address`, `self_about`, `profile_image`, `slug`, `active_status`, `online_status`, `approval_status`) VALUES
(8, 'Rafiq', 'Sarkar', '258963', '456465', 'imran@gmail.com', '01833452456', 'Male', '1990-06-02', 'NEUROLOGY', '131ddb54f8a94da689fb1565372cdfb9d33b2106', 'Dhaka', 'Dhaka', '', 'uploads/dc1.jpg', 'rafiq-sarkar', 1, 0, 1),
(9, 'Sakib', 'Sarkar', '258963', '456465', 'sakib@gmail.com', '01833452456', 'Male', '1990-06-02', 'NEUROLOGY', '131ddb54f8a94da689fb1565372cdfb9d33b2106', 'Dhaka', 'Dhaka', '', 'uploads/dc1.jpg', 'sakib-sarkar', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_job_history`
--

CREATE TABLE `doctor_job_history` (
  `doc_job_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `doc_office_name` varchar(250) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` varchar(80) NOT NULL,
  `office_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_job_history`
--

INSERT INTO `doctor_job_history` (`doc_job_id`, `d_id`, `doc_office_name`, `start_date`, `end_date`, `office_address`) VALUES
(12, 8, 'Labaid', '1970-01-01', 'Continuing', 'Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `dtrainings`
--

CREATE TABLE `dtrainings` (
  `dtr_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `training_title` varchar(255) NOT NULL,
  `institute_name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtrainings`
--

INSERT INTO `dtrainings` (`dtr_id`, `d_id`, `training_title`, `institute_name`, `address`, `description`) VALUES
(6, 8, 'Advance Surgery Using Technology adsfasdf', 'Dhaka Medical', 'Dhaka', '');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `pt_id` bigint(20) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(11) NOT NULL,
  `height` varchar(50) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `blood_group` varchar(50) NOT NULL,
  `nid` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` text NOT NULL,
  `birthday` date NOT NULL,
  `password` varchar(42) NOT NULL,
  `profile_img` varchar(255) CHARACTER SET ucs2 NOT NULL,
  `account_type` varchar(50) NOT NULL,
  `expire_start` datetime NOT NULL,
  `expire_date` varchar(10) NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`pt_id`, `first_name`, `last_name`, `father_name`, `mother_name`, `email`, `mobile_no`, `height`, `weight`, `blood_group`, `nid`, `gender`, `address`, `birthday`, `password`, `profile_img`, `account_type`, `expire_start`, `expire_date`, `active_status`, `create_date`) VALUES
(15, 'Azizul', 'Haque', 'Rafiqul Islam', 'Jahanara Begum', 'azizulhaque@yahoo.com', '01833565234', '5''5"', '68 Kg', 'AB', 'Na', 'Male', 'Joypurhat', '1992-10-04', '131ddb54f8a94da689fb1565372cdfb9d33b2106', '', 'Free Trial', '2017-08-10 19:17:21', '7', 0, '2017-08-10 13:17:21'),
(16, 'Riyad', 'Hossain', 'Rafiqul Islam', 'Jahanara Begum', 'riyad@yahoo.com', '01833565234', '5''5"', '68 Kg', 'AB', 'Na', 'Male', 'Joypurhat', '1992-10-04', '131ddb54f8a94da689fb1565372cdfb9d33b2106', '', 'Premium', '2017-08-10 19:17:21', '', 0, '2017-08-10 13:17:21');

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `id` int(11) NOT NULL,
  `pt_id` int(11) NOT NULL,
  `transection_id` varchar(255) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `package_day` varchar(20) NOT NULL,
  `approve_status` tinyint(4) DEFAULT '0',
  `pay_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`id`, `pt_id`, `transection_id`, `amount`, `package_day`, `approve_status`, `pay_date`) VALUES
(18, 16, '234234', '18000.00', '1 year', 0, '2017-08-10 19:55:39');

-- --------------------------------------------------------

--
-- Table structure for table `working_hours`
--

CREATE TABLE `working_hours` (
  `hour_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `day_name` varchar(20) NOT NULL,
  `from_to` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deducations`
--
ALTER TABLE `deducations`
  ADD PRIMARY KEY (`dedu_id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`d_id`);

--
-- Indexes for table `doctor_job_history`
--
ALTER TABLE `doctor_job_history`
  ADD PRIMARY KEY (`doc_job_id`);

--
-- Indexes for table `dtrainings`
--
ALTER TABLE `dtrainings`
  ADD PRIMARY KEY (`dtr_id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`pt_id`);

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `working_hours`
--
ALTER TABLE `working_hours`
  ADD PRIMARY KEY (`hour_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `deducations`
--
ALTER TABLE `deducations`
  MODIFY `dedu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `doctor_job_history`
--
ALTER TABLE `doctor_job_history`
  MODIFY `doc_job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `dtrainings`
--
ALTER TABLE `dtrainings`
  MODIFY `dtr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `pt_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `working_hours`
--
ALTER TABLE `working_hours`
  MODIFY `hour_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
