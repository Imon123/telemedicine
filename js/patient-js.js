(function($){
  $("form[name='patient_reg']").validate({
    // Specify validation rules
    rules: {
      pfirst_name: {
        required: true,
        alphas: true
      },
      plast_name: {
        required: true,
        alphas: true
      },
     pemail: {
        required: true,
        email: true
      },
      pmobile_no: {
        required: true,
        numeric:true
      },
      height: "required",
      weight: "required",
      pgender:"required",
      pbirthday: "required",
      address: "required",
      ppassword: {
        required: true,
        minlength: 5,
        maxlength: 15
      },
      repassword: {
          required: true,
          equalTo: "#ppassword"
      }
      
    },
    // Specify validation error messages
    messages: {
      pfirst_name: {
        required: "Please enter your firstname",
        alphas : "Please enter only alphabets and space."
      },
      plast_name: {
        required: "Please enter your lastname",
        alphas : "Please enter only alphabets and space."
      },
      pmobile_no: {
        required: "Please enter your phone no.",
        numeric:"Please enter a valid cell phone number. For example 019xxxxxxxx"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long",
        maxlength: "Your password must be less than 10 characters"
      },
      email:"Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
  submitHandler: function() {
    var f_name     = $('#pfirst_name').val();
    var l_name     = $('#plast_name').val();
    var fa_name     = $('#father_name').val();
    var mo_name     = $('#mother_name').val();
    var email      = $('#pemail').val();
    var mobile_no  = $('#pmobile_no').val();
    var height     = $('#height').val();
    var weight     = $('#weight').val();
    var bl_group   = $('#blood_group').val();
    var nid        = $('#nid').val();
    var gender     = $('#pgender').val();
    var address    = $('#address').val();
    var birthday   = $('#pbirthday').val();
    var password   = $('#ppassword').val();
    var acc_type   = $('#acc_type').val();
    var ptbtn       = $('#pt_button').val();
      $.ajax({
      url: 'ajax-action.php',
      data: 'f_name='+f_name+'&l_name='+l_name+'&fa_name='+fa_name+'&mo_name='+mo_name+'&email='+email+'&mobile_no='+mobile_no+'&height='+height+'&weight='+weight+'&bl_group='+bl_group+'&nid='+nid+'&birthday='+birthday+'&gender='+gender+'&address='+address+'&password='+password+'&acc_type='+acc_type+'&ptbtn='+ptbtn,
      type : 'POST',
      success:function(result){
        if(result == "checkmail"){
          $('#pt_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> You are already registered !</div>');
        }else if(result == "mail"){
              $("#ptsignup").remove();
              $('#pt_notify').html('<div class="alert success-color alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>You are Registered Successfully, Wait a second!<i class="fa fa-check-circle-o"></i> <img class="loading-img" src="images/loading.gif" alt=""></div>');
              function redirect_page()
              {
                document.location="login.php";
              }
              setTimeout(redirect_page,5000);
            /*if(result == "yes"){
              $("#ptsignup").remove();
              $('#pt_notify').html('<div class="success-color alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>You are Registered Successfully & We have sent activation link in your mail, Wait a minute!<i class="fa fa-check-circle-o"></i> <img class="loading-img" src="images/loading.gif" alt=""></div>');
              function redirect_page()
              {
                document.location="login.php";
              }
              setTimeout(redirect_page,5000);
            }else{
              $("#ptsignup").remove();
              $('#pt_notify').html('<div class="success-color alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>You are Registered Successfully. But we really apologize for won\'t be able to send activation link in your mail. Please contact our <a href="#">help center</a></div>');
            }*/
        }else{
            $("#ptsignup").remove();
            $('#pt_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> Error in registering...Please try again later!! </div>');
                function hide_message()
                {
                 $('#pt_notify').fadeOut('slow');
                 $('#modaldaftar').remove().fadeOut('slow');
                }
                setTimeout(hide_message,5000);
        }
      },
    });
    return false;             
  }

  });


 // patient login 

   $("form[name='patient-login']").validate({
    // Specify validation rules
    rules: {
      email: {
        required: true,
        email: true
      },

      password: {
        required: true
      }
      
    },
    // Specify validation error messages
    messages: {
    
      password: {
        required: "Please provide a password",
      },
      email:"Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
  submitHandler: function() {
    var email      = $('#email').val();
    var password   = $('#password').val();
    var plbtn       = $('#login-submit').val();
      $.ajax({
      url: 'ajax-action.php',
      data: 'email='+email+'&password='+password+'&plbtn='+plbtn,
      type : 'POST',
      success:function(result){
        if(result == "succesLogon"){
          $('#ptlogin_notify').html('<div class="alert success-color alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>You are Login Successfully, Wait a second!<i class="fa fa-check-circle-o"></i><div class="pt_login"><img class="loading-img" src="images/loading.gif" alt=""></div></div>');
          function redirect_page()
          {
            document.location="patient-panel";
          }
          setTimeout(redirect_page,5000);
        }else{
          $('#ptlogin_notify').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> Email Or Password Invalid... Please Try Again!! </div>');    
        }
      },
    });
    return false;             
  }

  });

 jQuery.validator.addMethod("alphas", function(value, element) {
      // allow any non-whitespace characters as the host part
        return this.optional(element ) || /^[a-zA-Z ]+$/.test( value );
      });
      jQuery.validator.addMethod("numeric", function(value, element) {
        // allow any non-whitespace characters as the host part
        return this.optional(element ) || /^[\-+]?[0-9]*\.?[0-9]+$/.test( value );
      });

var pvalue = 'premium';
$("#acc_type").val(pvalue);

$('#r1').click(function() { 
   if($('#r1').length){
    var pvalue = $('#r1').data('pvalue');
    $("#acc_type").val(pvalue);
  }
});
$('#r3').click(function() { 
   if($('#r3').length){
    var trial = $('#r3').data('trial');
    $("#acc_type").val(trial);
  }
});

}) ( jQuery );


(function($) {  
  //Date picker
    $('#birthday').datepicker();

  $("form[name='edit_patient_reg']").validate({
    // Specify validation rules
    rules: {
      first_name: {
        required: true,
        alphas: true
      },
      last_name: {
        required: true,
        alphas: true
      },
      father_name: {
        required: true,
        alphas: true
      },
      mother_name: {
        required: true,
        alphas: true
      },
      email: {
        required: true,
        email: true
      },
      mobile_no: {
        required: true,
        numeric:true
      },
      gender:"required",
      birthday: "required",
      address: "required",
      
    },
    // Specify validation error messages
    messages: {
      first_name: {
        required: "Please enter your firstname",
        alphas : "Please enter only alphabets and space."
      },
      last_name: {
        required: "Please enter your lastname",
        alphas : "Please enter only alphabets and space."
      },
      mobile_no: {
        required: "Please enter your phone no.",
        numeric:"Please enter a valid cell phone number. For example 019xxxxxxxx"
      },
      email:"Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();           
    }
  });


  // Patient Membership Expires system
  setInterval(function() {
        var pid = $('#patient_id').val();
        $.ajax({
        url:"../ajax-action.php?expire_member",
        type:"POST",
        data: {"pid":pid},
        success: function(result){
          $("#membership_expire").html(result);
        }
      });
     }, 3000);

 $("a#payment_btn").on('click', function(event) {
         event.preventDefault(); 
         alert("You did not login. Login please!!");
         window.location.href = "login.php";
  });


if($("div#pappintment a#firsttime_membership").length){
  $("div#pappintment a#firsttime_membership").show().fadeIn();
  $("div#pappintment #first_expired").hide().fadeOut();
  $("div#pappintment a#firsttime_membership").on('click', function(event){
   event.preventDefault(); 
    $(this).hide().fadeOut();
    $("div#pappintment #first_expired").show().fadeIn();
  });
}
if($("div#pappintment a#end_membrship").length){
  $("div#pappintment a#end_membrship").show().fadeIn();
  $("div#pappintment #pexpired").hide().fadeOut();
  $("div#pappintment a#end_membrship").on('click', function(event){
   event.preventDefault(); 
    $(this).hide().fadeOut();
    $("div#pappintment #pexpired").show().fadeIn();
  });
}

  $('input[name="radios"]').on('change', function() {
    
    if ($('#firstRadio').is(":checked")){

      $('#firstAccordion').collapse('show');
    
    } else {
      
      $('#firstAccordion').collapse('hide');
    }
        
    if ($('#secondRadio').is(":checked")){

      $('#secondAccordion').collapse('show');
    
    } else {
      
      $('#firstAccordion').collapse('hide');
    }

  });
  // New message count 
  setInterval(function(){
    var pid = $('#patient_id').val();
    $("#new_message").load("../ajax-action.php?newmessage="+pid);
  }, 5000); // 2 milisecond

})( jQuery );

// patient photo delete
function deletePatientPhoto(str){
  var pt_id = str;
  var checkstr =  confirm('are you sure you want to delete this?');
  if(checkstr == true){
      $.ajax({
      url: '../ajax-action.php?pphtodelete',
      data: "pt_id="+pt_id,
      type : 'POST',
      success:function(data){
        if(data == "yes"){
            alert("Photo has been deleted successfully");
            window.location="edit-patient-profile";
          }else{ 
            alert("Not deleted");
          }
      }
    });
  }else{
    return false;
  }
}

// add membership package
function ptpackageSubmit(){
  var pid = $('#patient_id').val();
  var first_radio = $('#firstRadio').val();
  var second_radio = $('#secondRadio').val();
  var months6 = $('#months6').val();
  var year1 = $('#year1').val();

  var radioName = 'radios';
  if(($('input[name='+ radioName +']:checked').length)){
    if(first_radio != '' && months6 !=''){
        var amount6 = $('#amount6').val();   
        $.ajax({
        url: '../ajax-action.php',
        data: 'pid='+pid+'&package='+first_radio+'&trans_id='+months6+'&amount='+amount6+'&packageadd',
        type : 'POST',
        success:function(result){
            if(result == "yes"){
              alert("Inserted Successfully");
          }else{
            alert('Not submitted... Please Try Again');   
          }
        }
      });
    }else if(second_radio != '' && year1 !=''){
       var amount1 = $('#amount1').val();   
        $.ajax({
        url: '../ajax-action.php',
        data: 'pid='+pid+'&package='+second_radio+'&trans_id='+year1+'&amount='+amount1+'&packageadd',
        type : 'POST',
        success:function(result){
            if(result == "yes"){
              alert("Inserted Successfully");
          }else{
            alert('Not submitted... Please Try Again');   
          }
        }
      });
    }else{
      alert('Field must not be empty');
    }
  }else{
    alert('Must checked at least one radion button');
  }
}



(function($){
 $("#uploadpimage").on('submit',(function(e) {
  e.preventDefault();

    $("#message").empty();
    $('#loading').show();
    var pt_id = $('#pt_id').val();
  $.ajax({
    url: "../ajax-action.php?pimgadd="+pt_id, // Url to which the request is send
    type: "POST",             // Type of request to be send, called as method
    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,        // To send DOMDocument or non processed data file it is set to false
    success: function(data)   // A function to be called if request succeeds
    {
      if(data !=''){
          $('#loading').hide();
          $("#message").html(data);
        // hide the message after 3ms
        setTimeout(window.location="edit-patient-profile", 10000);
      }
    }
  });
  }));
  // Function to preview image after validation
})( jQuery );
